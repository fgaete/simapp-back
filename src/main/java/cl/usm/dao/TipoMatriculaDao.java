/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.usm.dao;

import cl.usm.model.TipoMatriculaModel;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kguzman
 */
@Repository
@Transactional
public class TipoMatriculaDao {
    //@Autowired
    private SessionFactory sessionFactory;
    
    @SuppressWarnings("unchecked")
    public List<TipoMatriculaModel> list(){
        System.out.println("......");
        System.out.println("paso por list");
        String sql = "SELECT codMatricula, nomMatricula FROM TipoMatricula";
        List<TipoMatriculaModel> lista = new ArrayList<TipoMatriculaModel>();

        Session session = sessionFactory.getCurrentSession();
        lista = session.createQuery(sql).getResultList();
        session.close();

        return lista;
    }
    
    public TipoMatriculaModel get(int codTipoMatricula){
        return sessionFactory.getCurrentSession().get(TipoMatriculaModel.class, codTipoMatricula);
    }
}
