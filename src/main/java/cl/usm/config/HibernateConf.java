/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.usm.config;

import java.util.Properties;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.apache.tomcat.dbcp.dbcp.BasicDataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;


/**
 *
 * @author kguzman
 */
@Configuration
@EnableTransactionManagement
@ComponentScan({"cl.usm.config"})
public class HibernateConf {
    
    @Autowired
    private Environment env;
    
    @Bean
    public LocalSessionFactoryBean sessionFactory() throws NamingException {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan( "cl.usm.model" );
        sessionFactory.setHibernateProperties(hibernateProperties());
 
        return sessionFactory;
    }
    
    @Bean
    public DataSource dataSource() throws NamingException {
        
        //return (DataSource) new JndiTemplate().lookup(env.getProperty("java:comp/env/jdbc/OracleDS"));
        //"java:comp/env/jdbc/exampleDB"
        
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("oracle.jdbc.OracleDriver");
        dataSource.setUrl("jdbc:oracle:thin:@orcl-test.dti.utfsm.cl:1521:orcl");
        dataSource.setUsername("DBO_DB_UTFSM");
        dataSource.setPassword("produccion");
        
        return dataSource;
    }
    
 
    public Properties hibernateProperties() {
        Properties pro = new Properties();
        pro.setProperty("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect");
        pro.setProperty("current_session_context_class", "thread");
        pro.setProperty("hibernate.show_sql", "false");
        
        /*
        return new Properties() {
         {
            setProperty("hibernate.dialect", env.getProperty("org.hibernate.dialect.Oracle10gDialect"));
            setProperty("current_session_context_class", env.getProperty("thread"));
            setProperty("hibernate.show_sql", env.getProperty("false"));
         }*/
        
        return pro;
    }
    
    @Bean
    public PlatformTransactionManager hibernateTransactionManager() throws NamingException {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager();
        transactionManager.setSessionFactory(sessionFactory().getObject());
        return transactionManager;
    }
    /*
    @Bean
    public TomcatServletWebServerFactory tomcatFactory() {
        return new TomcatServletWebServerFactory() {
            @Override
            protected TomcatWebServer getTomcatWebServer(org.apache.catalina.startup.Tomcat tomcat) {
                tomcat.enableNaming();
                return super.getTomcatWebServer(tomcat);
            }

            @Override
            protected void postProcessContext(Context context) {
                ContextResource resource = new ContextResource();           
                //resource.setProperty("factory", "org.apache.tomcat.jdbc.pool.DataSourceFactory");
                resource.setName("jdbc/OracleDS");
                resource.setType(DataSource.class.getName());
                resource.setProperty("driverClassName", "oracle.jdbc.OracleDriver");
                resource.setProperty("url", "db_url");
                
                context.getNamingResources().addResource(resource);
            }
        };
    
    }*/
    
    
    
    
}
