/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.usm.service;

import cl.usm.dao.TipoMatriculaDao;
import cl.usm.model.TipoMatriculaModel;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kguzman
 */
@Service
@Transactional(readOnly = true)
public class TipoMatriculaService {
    @Autowired
    private TipoMatriculaDao tipoMatriculaDao;
    
    @Transactional
    public List<TipoMatriculaModel> list(){
        System.out.println("--- list service ---");
        return tipoMatriculaDao.list();
    }
    
   @Transactional
    public TipoMatriculaModel get(int codTipoMatricula){
        return tipoMatriculaDao.get(codTipoMatricula);
    }
}
