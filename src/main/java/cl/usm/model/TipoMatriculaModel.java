/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.usm.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author kguzman
 */

@Entity
@Table(name="TIPO_MATRICULA", schema="DBO_DB_UTFSM")
public class TipoMatriculaModel implements java.io.Serializable{
    
     private int codMatricula;
     private String nomMatricula;
     private Long rutUsuario;
     private Date fechaModificacion;

    public TipoMatriculaModel() {
    }

	
    public TipoMatriculaModel(int codMatricula) {
        this.codMatricula = codMatricula;
    }
    public TipoMatriculaModel(int codMatricula, String nomMatricula, Long rutUsuario, Date fechaModificacion) {
       this.codMatricula = codMatricula;
       this.nomMatricula = nomMatricula;
       this.rutUsuario = rutUsuario;
       this.fechaModificacion = fechaModificacion;
    }
   
     @Id 

    
    @Column(name="COD_MATRICULA", unique=true, nullable=false, precision=7, scale=0)
    public int getCodMatricula() {
        return this.codMatricula;
    }
    
    public void setCodMatricula(int codMatricula) {
        this.codMatricula = codMatricula;
    }

    
    @Column(name="NOM_MATRICULA", length=50)
    public String getNomMatricula() {
        return this.nomMatricula;
    }
    
    public void setNomMatricula(String nomMatricula) {
        this.nomMatricula = nomMatricula;
    }

    
    @Column(name="RUT_USUARIO", precision=12, scale=0)
    public Long getRutUsuario() {
        return this.rutUsuario;
    }
    
    public void setRutUsuario(Long rutUsuario) {
        this.rutUsuario = rutUsuario;
    }

    @Temporal(TemporalType.DATE)
    @Column(name="FECHA_MODIFICACION", length=7)
    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }
    
    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }



}
