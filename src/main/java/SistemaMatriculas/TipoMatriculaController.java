/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SistemaMatriculas;

import cl.usm.model.TipoMatriculaModel;
import cl.usm.service.TipoMatriculaService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author kguzman
 */
@Controller
@CrossOrigin(origins= "*", methods={RequestMethod.GET, RequestMethod.POST})
public class TipoMatriculaController {
    
    @Autowired
    private TipoMatriculaService tipoMatriculaService;
    
    /*--- obtiene todos los tipos de matrículas ---*/
    @GetMapping(value ="/tiposMatricula" ,  headers="Accept=application/json")
    public ResponseEntity<?> list() {
        
        try{
            System.out.println("--- Empieza el proceso ---");
            List<TipoMatriculaModel> tiposMatricula = new ArrayList<TipoMatriculaModel>();
            tipoMatriculaService.list();
            
            return ResponseEntity.ok().body(tiposMatricula);
        }catch(Exception ex){
            System.out.println("---- Exception ----1");
            System.out.println(ex.getMessage());
            System.out.println("---------------------------1");
            return ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .body(ex.toString());
        }
        
    }   
    
    @GetMapping(value = "/tiposMatricula/{codTipoMatricula}", headers = "Accept=application/json")
    public ResponseEntity<?> get(@PathVariable int codTipoMatricula ){
        try{
            return ResponseEntity.ok().body(tipoMatriculaService.get(codTipoMatricula));
        } catch(NullPointerException ex){
            return ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .body(ex);
        }
    }
    
}
