/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SistemaMatriculas;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 *
 * @author kguzman
 */
@SpringBootApplication (scanBasePackages={"cl.usm.model", "cl.usm.service", "cl.usm.config", "cl.usm.dao", "cl.usm.util" , "SistemaMatriculas"})
@ComponentScan({"cl.usm.model", "cl.usm.service", "cl.usm.config", "cl.usm.dao", "cl.usm.util" , "SistemaMatriculas"})
public class Main extends SpringBootServletInitializer{
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        SpringApplication.run(Main.class, args); //new AnnotationConfigApplicationContext(Main.class);
        
        /*
        for (String beanName : applicationContext.getBeanDefinitionNames()) {
            System.out.println(beanName);
        }*/
        
    }
    
    
}
